# Importar la librería de acceso a Postgres
import psycopg2
from datos_conexion import dc

def consultar_id():
    try:
        #Se crea la conexión a la base de datos
        conexion = psycopg2.connect(**dc)
        #Se crea el cursor
        cursor = conexion.cursor()
        #Sentencia SQL

        #Se hace un ciclo para verificar que el dato ingresado sea un número

        validar_id=False
        while validar_id==False:
            id=input('Digite el id del proyecto: ')
            if id.isdecimal()==True:
                validar_id=True
            else:
                print("Debe de digitarse un valor numérico")

        cursor.execute('select id from proyectos')
        lista=cursor.fetchall()
        lista_id=[]
        for iterador in lista:
            lista_id.append(iterador[0])

        variable_validacion=False

        for iterador in lista_id:
            if int(id)==iterador:
                variable_validacion=True

        if variable_validacion == True:
            print("El id del proyecto se encontró en la base de datos\n")
            return id
        else:
            print("El id del proyecto no se encontró en la base de datos\n")
            return -1

        #Se cierra el cursor y la conexión con la base de datos
        cursor.close()
        conexion.close()

    except:
        print('La conexión con la base de datos ha fallado')
        exit()
        return -1

