# Proyecto Scrum

Este proyecto tiene como objetivo desarrollar la versión inicial del módulo de
proyectos y User Stories del Sistema OnLine de Scrum para una empresa.

## Herramientas a usar

-VSCode
-Git
-BitBucket
-Python 3.9
-AWS
-Postgres
-Amazon RDS



Programa desarrollado por los estudiantes:

José Rodrigo Ávila Castro.
David Santiago Avilan López.
Laura Sofía Rodríguez Castillo.
Daniel Enrique Pérez Cicua.
Camilo Andrés Vergara García.

Pertenecientes a la facultad de ingenieria de Telecomunicaciones de la
universidad Santo Tomás.